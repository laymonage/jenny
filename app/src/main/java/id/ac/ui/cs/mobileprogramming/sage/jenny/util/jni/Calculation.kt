package id.ac.ui.cs.mobileprogramming.sage.jenny.util.jni

class Calculation {
    companion object {
        init {
            System.loadLibrary("native-lib")
        }
    }

    external fun add(a: Double, b: Double): Double
    external fun subtract(a: Double, b: Double): Double
    external fun multiply(a: Double, b: Double): Double
    external fun divide(a: Double, b: Double): Double
}
