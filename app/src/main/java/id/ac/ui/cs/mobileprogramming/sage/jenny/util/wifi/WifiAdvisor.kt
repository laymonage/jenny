package id.ac.ui.cs.mobileprogramming.sage.jenny.util.wifi

import android.app.Activity
import android.content.Context.WIFI_SERVICE
import android.net.wifi.WifiConfiguration
import android.net.wifi.WifiManager
import android.net.wifi.WifiNetworkSuggestion
import android.os.Build

class WifiAdvisor(private val activity: Activity) {
    fun setWifi(ssid: String, key: String) {
        val wifiManager = activity.applicationContext.getSystemService(WIFI_SERVICE) as WifiManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val wifiConfiguration = WifiNetworkSuggestion.Builder()
            wifiConfiguration.setSsid(ssid)
            if (key.isBlank()) {
                wifiConfiguration.setWpa2Passphrase(key)
            }
            wifiManager.addNetworkSuggestions(mutableListOf(wifiConfiguration.build()))
        } else {
            wifiManager.isWifiEnabled = true
            val wifiConfiguration = WifiConfiguration()
            wifiConfiguration.SSID = "\"$ssid\""
            if (key.isNotBlank()) {
                wifiConfiguration.preSharedKey = "\"$key\""
            }
            wifiManager.disconnect()
            val netId = wifiManager.addNetwork(wifiConfiguration)
            wifiManager.enableNetwork(netId, true)
            wifiManager.reconnect()
        }
    }
}
