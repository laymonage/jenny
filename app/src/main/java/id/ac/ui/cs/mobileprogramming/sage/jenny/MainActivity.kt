package id.ac.ui.cs.mobileprogramming.sage.jenny

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import id.ac.ui.cs.mobileprogramming.sage.jenny.util.jni.Calculation
import id.ac.ui.cs.mobileprogramming.sage.jenny.util.wifi.WifiAdvisor
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private val calculation = Calculation()
    private lateinit var wifiAdvisor: WifiAdvisor

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        wifiAdvisor = WifiAdvisor(this)

        buttonAdd.setOnClickListener {
            val (a, b) = getAB()
            result.text = calculation.add(a, b).toString()
        }
        buttonSubtract.setOnClickListener {
            val (a, b) = getAB()
            result.text = calculation.subtract(a, b).toString()
        }
        buttonMultiply.setOnClickListener {
            val (a, b) = getAB()
            result.text = calculation.multiply(a, b).toString()
        }
        buttonDivide.setOnClickListener {
            val (a, b) = getAB()
            result.text = calculation.divide(a, b).toString()
        }

        buttonConnect.setOnClickListener {
            val (ssid, key) = getSsidKey()
            wifiAdvisor.setWifi(ssid, key)
        }
    }

    private fun getAB() = Pair(
        inputA.text.toString().toDoubleOrNull() ?: 0.0,
        inputB.text.toString().toDoubleOrNull() ?: 0.0
    )

    private fun getSsidKey() = Pair(inputSsid.text.toString(), inputKey.text.toString())
}
