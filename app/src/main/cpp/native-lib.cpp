#include <jni.h>
#include <string>

extern "C" JNIEXPORT jdouble JNICALL
Java_id_ac_ui_cs_mobileprogramming_sage_jenny_util_jni_Calculation_add(
        JNIEnv *env,
        jobject,
        jdouble a,
        jdouble b) {
    return a+b;
}

extern "C" JNIEXPORT jdouble JNICALL
Java_id_ac_ui_cs_mobileprogramming_sage_jenny_util_jni_Calculation_subtract(
        JNIEnv *env,
        jobject,
        jdouble a,
        jdouble b) {
    return a-b;
}

extern "C" JNIEXPORT jdouble JNICALL
Java_id_ac_ui_cs_mobileprogramming_sage_jenny_util_jni_Calculation_multiply(
        JNIEnv *env,
        jobject,
        jdouble a,
        jdouble b) {
    return a*b;
}

extern "C" JNIEXPORT jdouble JNICALL
Java_id_ac_ui_cs_mobileprogramming_sage_jenny_util_jni_Calculation_divide(
        JNIEnv *env,
        jobject,
        jdouble a,
        jdouble b) {
    return a/b;
}
